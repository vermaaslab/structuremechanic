#==============================================================================
# TOP LEVEL SETUP
#==============================================================================
#------------------------------------------------------------------------------
namespace eval ::structuremechanic {}
#------------------------------------------------------------------------------
# top level API
proc rotatebond {idxlist idx idx2 rot {mid top} {f now}} {
	::structuremechanic::procs::rotatebond $idxlist $idx $idx2 $rot $mid $f
}
#------------------------------------------------------------------------------
proc autorotatebond {idx1 idx2 rot {mid top} {f now}} {
	::structuremechanic::procs::autorotatebond $idx1 $idx2 $rot $mid $f	
}
#------------------------------------------------------------------------------
proc rotateGUI {} {
	eval ::structuremechanic::gui::structuremechanic_gui
}
#------------------------------------------------------------------------------
#==============================================================================



#==============================================================================
# PROCS
#==============================================================================
#------------------------------------------------------------------------------
namespace eval ::structuremechanic::procs {}
#------------------------------------------------------------------------------
#Called internally by autorotatebond
proc ::structuremechanic::procs::rotatebond {idxlist idx idx2 rot {mid top} {f now}} {
	#idxlist is the list of indices that will get rotated. idx is bonded to an element in idxlist, and that is the bond around which we will be rotating.
	set rotatesel [atomselect $mid "index $idxlist" frame $f]
	set basesel [atomselect $mid "index $idx" frame $f]
	set partner [atomselect $mid "index $idx2" frame $f]
	if {[$basesel num] != 1} {
		puts "Second argument results in a atomselection of the wrong size."
		puts "Atomselection size: [$basesel num]"
		return
	}
	if {[$partner num] != 1} {
		puts "Third argument results in a atomselection of the wrong size."
		puts "Atomselection size: [$parter num]"
		return
	}
	set M [trans bond  [lindex [$basesel get {x y z}] 0] [lindex [$partner get {x y z}] 0] $rot deg]
	$rotatesel move $M
	$partner delete
	$rotatesel delete
	$basesel delete
}
#------------------------------------------------------------------------------
#Split a bond, and see which side is bigger. We'll be moving the smaller of the two sides.
proc ::structuremechanic::procs::selectsmaller { idx1 idx2 {mid top} } {
	set sel1 [atomselect $mid "index $idx1"]
	set sel2 [atomselect $mid "index $idx2"]
	#Expand the tree.
	set newsel1 [atomselect $mid "index $idx1 [join [$sel1 getbonds]] and not index $idx2"]
	set nidx1 [$newsel1 get index]
	set newsel2 [atomselect $mid "index $idx2 [join [$sel2 getbonds]] and not index $nidx1"]
	if { [$sel1 num] == [$newsel1 num] || [$sel2 num] == [$newsel2 num]} {
		puts "Bond not rotateable"
		return
	}
	#Keep expanding the selections until we are out of stuff to expand to on one side.
	while { [$sel1 num] != [$newsel1 num] && [$sel2 num] != [$newsel2 num] } {
		$sel1 delete
		$sel2 delete
		set sel1 $newsel1
		set sel2 $newsel2
		set nidx2 [$newsel2 get index]
		set newsel1 [atomselect $mid "index $nidx1 [join [$sel1 getbonds]] and not index $nidx2"]
		set nidx1 [$newsel1 get index]
		set newsel2 [atomselect $mid "index $nidx2 [join [$sel2 getbonds]] and not index $nidx1"]
	}
	set nidx2 [$newsel2 get index]
	if { [$sel1 num] == [$newsel1 num] } {
		set retval [list $idx2 $idx1 $nidx1]
	} else {
		set retval [list $idx1 $idx2 $nidx2]
	}
	#Cleanup.
	$sel1 delete
	$sel2 delete
	$newsel1 delete
	$newsel2 delete
	return $retval
}
#------------------------------------------------------------------------------
#This is the guy that rotates a bond based on two indices.
proc ::structuremechanic::procs::autorotatebond {idx1 idx2 rot {mid top} {f now}} {
	#Designed to be the "lazy" way of rotating a bond. Just give it two bonded atoms and a rotation, and it'll figure out the rest!
	lassign [::structuremechanic::procs::selectsmaller $idx1 $idx2 $mid] idx i ilist
	::structuremechanic::procs::rotatebond $ilist $idx $i $rot $mid $f
}
proc ::structuremechanic::procs::autosetbondlength {idx1 idx2 length {mid top} {f now}} {
	lassign [::structuremechanic::procs::selectsmaller $idx1 $idx2 $mid] idx i ilist
	set fixedatom [atomselect $mid "index $idx"]
	set bondedatom [atomselect $mid "index $i"]
	set movingatoms [atomselect $mid "index $ilist"]
	set vec [vecsub [lindex [$bondedatom get {x y z}] 0] [lindex [$fixedatom get {x y z}] 0]]
	set movevec [vecscale [vecnorm $vec] [expr {$length - [veclength $vec]}]]
	$movingatoms moveby $movevec
	$fixedatom delete
	$bondedatom delete
	$movingatoms delete
}
#idx0 is the center atom, 1 and 2 are left and right, respectively.
proc ::structuremechanic::procs::autoanglerotate {idx0 idx1 idx2 rot {mid top} {f now}} {
	lassign [::structuremechanic::procs::selectsmaller $idx1 $idx2 $mid] idx i ilist
	set lsix [lsearch $ilist $idx0]
	#Make sure that the central atom is NOT included in the indexlist of stuff that will move.
	if { $lsix != -1 } {
		set ilist [lreplace $ilist $lsix $lsix
	}
	set c [atomselect $mid "index $idx0"]
	set l [atomselect $mid "index $idx"]
	set r [atomselect $mid "index $i"]
	set movingatoms [atomselect $mid "index $ilist"]
	set M [trans angle [lindex [$r get {x y z}] 0] [lindex [$c get {x y z}] 0] [lindex [$l get {x y z}] 0] $rot deg]
	$movingatoms move $M
	$c delete
	$l delete
	$r delete
	$movingatoms delete
}
#------------------------------------------------------------------------------
#"Magically" picks an arbitrary dihedral. Only if it is interesting.
proc ::structuremechanic::procs::pickdihedral {idx1 idx2 {mid top} {f now}} {
	set sel1 [atomselect $mid "index $idx1"]
	set sel2 [atomselect $mid "index $idx2"]
	set idx0sel [atomselect $mid "index [join [$sel1 getbonds]] and not index $idx2"]
	set idx3sel [atomselect $mid "index [join [$sel2 getbonds]] and not index $idx1"]
	$sel1 delete
	$sel2 delete
	if { [$idx0sel num] == 0 || [$idx3sel num] == 0} {
		$idx0sel delete
		$idx3sel delete
		return [list ]
	}
	#Find the most massive element bonded to idx1 and idx2 to form idx0 and idx3 respectively.
	set idx0 [lindex [$idx0sel get index] [lsearch [lsort -indices -real -decreasing [$idx0sel get mass]] 0]]
	set idx3 [lindex [$idx3sel get index] [lsearch [lsort -indices -real -decreasing [$idx3sel get mass]] 0]]
	$idx0sel delete
	$idx3sel delete
	return [list $idx0 $idx1 $idx2 $idx3]
}
#------------------------------------------------------------------------------
proc ::structuremechanic::test {} {
	mol new 2L6X first 0 last 0
	for { set i 0 } { $i < 360 } { incr i } {
		animate dup 0
		rotatebond [[atomselect top "resid 34 and sidechain"] get index] [[atomselect top "resid 34 and name CA"] get index] [[atomselect top "resid 34 and name CB"] get index] 1
	}
	for { set i 0 } { $i < 360 } { incr i } {
		animate dup 0
		#index 207 and 208 are in resid 34
		autorotatebond 207 208 1
	}
	animate dup 0
	::structuremechanic::procs::autosetbondlength 207 208 100
	for { set i 0 } { $i < 10 } { incr i } {
		animate dup 0
		#This is at the end of resid 35
		::structuremechanic::procs::autoanglerotate 232 233 234 45
	}
	for { set i 0 } { $i < 10 } { incr i } {
		animate dup 0
		#This is at the end of resid 36
		::structuremechanic::procs::autoanglerotate 250 251 252 45
	}
	for { set i 0 } { $i < 10 } { incr i } {
		animate dup 0
		#This is at the end of resid 37
		::structuremechanic::procs::autoanglerotate 266 268 267 45
	}
}
#------------------------------------------------------------------------------
#==============================================================================



#==============================================================================
# GUI Code
#==============================================================================
#------------------------------------------------------------------------------
namespace eval ::structuremechanic::gui {
	variable w

	variable idx1_indices
	variable idx2_indices
	variable molid "top"
	variable frame "now"
	variable step "10"
	variable runmode "manual"
	#This is the unified atomselection between the two tabs.
	variable atomsel
	#Index currently shown in the center, left, and right dropdown boxes
	variable idc
	variable idl
	variable idr
}
#Based on the selection, return a list of ids.
proc ::structuremechanic::gui::centerboxcontents {} {
	return $atomsel get index
}
proc ::structuremechanic::gui::leftboxcontents {} {
	if { $idc == "" } {
		return [list ]
	}
	set tmpsel [atomselect $molid "index $idc"]
	set retlist [join [$tmpsel getbonds]]
	$tmpsel delete
	return $retlist
}
proc ::structuremechanic::gui::rightboxcontents {} {
	if { $idc == "" || $idl == "" } {
		return [list ]
	}
	set tmpsel [atomselect $molid "index $idc"]
	set retlist [join [$tmpsel getbonds]]
	#Remove the index in the left box.
	set li [lsearch $retlist $idl]
	set retlist [lreplace $retlist $li $li]
	$tmpsel delete
	return $retlist
}
#------------------------------------------------------------------------------
proc ::structuremechanic::gui::structuremechanic_gui {} {
	# launches GUI for controlling molecular rotations

	# STYLE SETUP
	# set variables for controlling element paddings (style)
    set vbuttonPadX 5; # vertically aligned std buttons
    set vbuttonPadY 0 
    set hbuttonPadX "5 0"; # horzontally aligned std buttons
    set hbuttonPadY 0
    set buttonRunPadX 10; # large buttons that launch procs
    set buttonRunPadY "10 10"
    set entryPadX 0; # single line entry
    set entryPadY 0
    set hsepPadX 10; # horizontal separators
    set hsepPadY 10
    set vsepPadX 0; # vertical separators
    set vsepPadY 0
    set labelFramePadX 0; # label frames
    set labelFramePadY "10 0"
    set labelFrameInternalPadding 5

    # define some special symbols that are commonly used
    set degree \u00B0

    # setup theme preferences based on availability
    set themeList [ttk::style theme names]
    if { [lsearch -exact $themeList "aqua"] != -1 } {
        ttk::style theme use aqua
        set placeHolderPadX 18
    } elseif { [lsearch -exact $themeList "clam"] != -1 } {
        ttk::style theme use clam
    } elseif { [lsearch -exact $themeList "classic"] != -1 } {
        ttk::style theme use classic
    } else {
        ttk::style theme use default
    }

    # TOP LEVEL WINDOW SETUP
    # localize / initialize variables
    variable w
    if { [winfo exists .structuremechanic_gui] } {
    	wm deiconify .structuremechanic_gui
    	return
    }
    set w [toplevel ".structuremechanic_gui"]
    wm title $w "structuremechanic GUI"
    # allow top window to expand/contract with .
    grid columnconfigure $w 0 -weight 1
    grid rowconfigure    $w 0 -weight 1
    # define an initial geometry
    wm geometry $w 300x160
    

    # WINDOW COMPONENT SETUP
    # build a high level frame (hlf)
    ttk::frame $w.hlf
    grid $w.hlf -column 0 -row 0 -sticky nswe
    # allow hlf to resize with w
    grid columnconfigure $w.hlf 0 -weight 1
    grid rowconfigure    $w.hlf 0 -weight 1

    # construct frame
    ttk::frame $w.hlf.topframe

    ttk::label $w.hlf.topframe.lbl_idx1 -text "idxlist/idx1:"
    ttk::entry $w.hlf.topframe.entry_idx1 -textvariable ::structuremechanic::gui::idx1_indices
    ttk::label $w.hlf.topframe.lbl_idx2 -text "idx2:"
    ttk::entry $w.hlf.topframe.entry_idx2 -textvariable ::structuremechanic::gui::idx2_indices

    ttk::frame $w.hlf.topframe.container
    ttk::label $w.hlf.topframe.container.lbl_mol -text "molid:"
    ttk::entry $w.hlf.topframe.container.entry_mol -textvariable ::structuremechanic::gui::molid
    ttk::label $w.hlf.topframe.container.lbl_frame -text "frame:"
    ttk::entry $w.hlf.topframe.container.entry_frame -textvariable ::structuremechanic::gui::frame
    ttk::label $w.hlf.topframe.container.lbl_step -text "step (${degree}):"
    ttk::entry $w.hlf.topframe.container.entry_step -textvariable ::structuremechanic::gui::step
    ttk::label $w.hlf.topframe.container.lbl_mode -text "mode:"
    ttk::menubutton $w.hlf.topframe.container.mb_mode -direction below -menu $w.hlf.topframe.container.mb_mode.menu -textvariable ::structuremechanic::gui::runmode
    	menu $w.hlf.topframe.container.mb_mode.menu -tearoff no
    	$w.hlf.topframe.container.mb_mode.menu add command -label "manual"    -command { set ::structuremechanic::gui::runmode "manual" }
    	$w.hlf.topframe.container.mb_mode.menu add command -label "automatic" -command { set ::structuremechanic::gui::runmode "auto" }

    ttk::button $w.hlf.topframe.rotate -text "ROTATE" \
    	-command {
    		switch $::structuremechanic::gui::runmode {
    			{manual} { ::structuremechanic::procs::rotatebond $::structuremechanic::gui::idx1_indices $::structuremechanic::gui::idx2_indices $::structuremechanic::gui::step $::structuremechanic::gui::molid $::structuremechanic::gui::frame }
    			{auto}   { ::structuremechanic::procs::autorotatebond $::structuremechanic::gui::idx1_indices $::structuremechanic::gui::idx2_indices $::structuremechanic::gui::step $::structuremechanic::gui::molid $::structuremechanic::gui::frame }
    			default  { puts "unknown runmode"; flush stdout }
    		}
    	}

    # grid frame
    grid $w.hlf.topframe -column 0 -row 0 -sticky nswe

	    grid $w.hlf.topframe.lbl_idx1   -column 0 -row 0 -sticky nsw
	    grid $w.hlf.topframe.entry_idx1 -column 1 -row 0 -sticky nswe -padx $entryPadX -pady $entryPadY
	    grid $w.hlf.topframe.lbl_idx2   -column 0 -row 1 -sticky nsw
	    grid $w.hlf.topframe.entry_idx2 -column 1 -row 1 -sticky nswe -padx $entryPadX -pady $entryPadY

	    grid $w.hlf.topframe.container  -column 0 -row 2 -sticky nswe -columnspan 2
		    grid $w.hlf.topframe.container.lbl_mol     -column 0 -row 0 -sticky nsw
		    grid $w.hlf.topframe.container.entry_mol   -column 1 -row 0 -sticky nswe -padx $entryPadX -pady $entryPadY
		    grid $w.hlf.topframe.container.lbl_frame   -column 2 -row 0 -sticky nsw
		    grid $w.hlf.topframe.container.entry_frame -column 3 -row 0 -sticky nswe -padx $entryPadX -pady $entryPadY
		    grid $w.hlf.topframe.container.lbl_step    -column 0 -row 1 -sticky nsw
		    grid $w.hlf.topframe.container.entry_step  -column 1 -row 1 -sticky nswe -padx $entryPadX -pady $entryPadY
		    grid $w.hlf.topframe.container.lbl_mode    -column 2 -row 1 -sticky nsw
		    grid $w.hlf.topframe.container.mb_mode     -column 3 -row 1 -sticky nswe

	    grid $w.hlf.topframe.rotate     -column 0 -row 3 -sticky nswe -columnspan 2 -padx $buttonRunPadX -pady $buttonRunPadY
    
    # configure frame
    grid columnconfigure $w.hlf.topframe 0 -weight 0
    grid columnconfigure $w.hlf.topframe 1 -weight 1

    grid columnconfigure $w.hlf.topframe.container {0 2} -weight 0
    grid columnconfigure $w.hlf.topframe.container {1 3} -weight 1

    grid rowconfigure $w.hlf.topframe 3 -minsize 60 -weight 0

}
#------------------------------------------------------------------------------
#==============================================================================
#::structuremechanic::test